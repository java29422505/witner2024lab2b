	import java.util.Scanner;
	public class GameLauncher{
		public static void main (String[] args){

			// this scanner will provide the user to make a choice. 

			Scanner reader = new Scanner(System.in);

			System.out.println("Hi!, please tell me which game would you like to play. To play Hangman please enter: 1 or 2 for Wordle");

			// User will need to make a choice between 1 and 2

			int userChoice = reader.nextInt();

			// if the choice is 1 then play Hungman Game.

			if (userChoice == 1){

			//Get user input
			System.out.println("Enter a 4-letter word:");

			String word = reader.next();

			//Convert to upper case

			word = word.toUpperCase();
			
			//Start hangman game

				Hungman.runGame(word);

			}
			// else if the user enters 2 then GameLauncher starts Wordle

			else if (userChoice ==2){

			String answer = Wordle.generateWord();

				Wordle.runGame(answer);
			}

			// else user enters anything else then it will aske again for new choice.

			else {

				System.out.println("you have entered wrong choice :(");

				// i wanted to make this more but unfortunately i have no more time.
			}


		}
	}